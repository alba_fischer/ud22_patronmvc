
CREATE DATABASE IF NOT EXISTS `MVC_01`;
USE `MVC_01`;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (nombre, apellido, direccion, dni) VALUES 
('Alba', 'Carles', 'Av Codonyol', '45896325'),
('Joan', 'Balada', 'Av Constitució', '32569854'),
('Raquel', 'Llorca', 'Av Catalunya', '45698235'),
('Esther', 'Gras', 'Passeig de Sant Joan', '75896321'),
('Arnau', 'Morancho', 'Av Paral·lel', '33652147'),
('Javier', 'Uviña', 'Av de Les Corts', '47859632'),
('Laia', 'Beltran', 'Plaça Catalunya', '15264352');

