package UD22.UD22_Exercici01;


import controller.ClienteController;
import model.service.ClienteServ;
import views.Buscar;
import views.MenuPrincipal;
import views.Registrar;
import views.Eliminar;


public class mainApp {
	ClienteServ miclienteServ;
	MenuPrincipal menuPrincipal;
	Buscar buscar;
	Registrar registro;
	Eliminar eliminar;
	ClienteController clienteController;
	
	
	public static void main(String[] args) {
		mainApp miPrincipal=new mainApp();
		miPrincipal.iniciar();
	}


	private void iniciar() {
		/*Se instancian las clases*/
		menuPrincipal=new MenuPrincipal();
		registro=new Registrar();
		buscar= new Buscar();
		eliminar= new Eliminar();
		miclienteServ=new ClienteServ();
		clienteController= new ClienteController();
		/*Se establecen las relaciones entre clases*/
		menuPrincipal.setCoordinador(clienteController);
		registro.setCoordinador(clienteController);
		buscar.setCoordinador(clienteController);
		eliminar.setCoordinador(clienteController);
		miclienteServ.setclienteController(clienteController);
		
		/*Se establecen relaciones con la clase coordinador*/
		clienteController.setMenuPrincipal(menuPrincipal);
		clienteController.setRegistrar(registro);
		clienteController.setEliminar(eliminar);
		clienteController.setClienteServ(miclienteServ);
		clienteController.setBuscar(buscar);		

		menuPrincipal.setVisible(true);
}

}


