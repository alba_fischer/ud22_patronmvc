package views;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import controller.ClienteController;
import model.dto.Cliente;
import model.service.ClienteServ;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Buscar extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JTextField textCodigo;
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textDireccion;
	private JTextField textDNI;
	private ClienteController clienteController;
	private JButton btnModificar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JButton btnCancelar;
	
	public Buscar() {
		setTitle("Vista buscar");
		setBounds(100, 100, 450, 326);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblAdministrarClientes = new JLabel("ADMINISTRAR CLIENTES");
		lblAdministrarClientes.setBounds(70, 27, 299, 46);
		lblAdministrarClientes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		getContentPane().add(lblAdministrarClientes);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(33, 86, 56, 16);
		getContentPane().add(lblCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setText("");
		textCodigo.setColumns(10);
		textCodigo.setBounds(101, 83, 97, 22);
		getContentPane().add(textCodigo);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(33, 132, 56, 16);
		getContentPane().add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setText("");
		textNombre.setColumns(10);
		textNombre.setBounds(101, 129, 97, 22);
		getContentPane().add(textNombre);
		
		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setBounds(235, 132, 56, 16);
		getContentPane().add(lblDNI);
		
		textDNI = new JTextField();
		textDNI.setText("");
		textDNI.setColumns(10);
		textDNI.setBounds(313, 129, 97, 22);
		getContentPane().add(textDNI);
		
		JLabel lblDireccion = new JLabel("Dirección");
		lblDireccion.setBounds(235, 176, 56, 16);
		getContentPane().add(lblDireccion);
		
		textDireccion = new JTextField();
		textDireccion.setText("");
		textDireccion.setColumns(10);
		textDireccion.setBounds(313, 173, 97, 22);
		getContentPane().add(textDireccion);
		
		textApellido = new JTextField();
		textApellido.setText("");
		textApellido.setColumns(10);
		textApellido.setBounds(101, 173, 97, 22);
		getContentPane().add(textApellido);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(33, 176, 56, 16);
		getContentPane().add(lblApellido);
		
		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilita(false, true, true, true, true, false, true, false);
				
			}
		});
		btnModificar.setBounds(166, 228, 97, 25);
		getContentPane().add(btnModificar);
		
		btnGuardar = new JButton();
		btnGuardar.setText("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Cliente miCliente=new Cliente();
					miCliente.setIdCliente(Integer.parseInt(textCodigo.getText()));
					miCliente.setNombre(textNombre.getText());
					miCliente.setApellido(textApellido.getText());
					miCliente.setDireccion(textDireccion.getText());
					miCliente.setDni(Integer.parseInt(textDNI.getText()));

					clienteController.modificarCliente(miCliente);
					
					if (ClienteServ.modificaCliente==true) {
						habilita(true, false, false, false, false, true, false, true);	
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnGuardar.setBounds(57, 228, 97, 25);
		getContentPane().add(btnGuardar);
		
		btnCancelar = new JButton();
		btnCancelar.setText("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		btnCancelar.setBounds(275, 228, 97, 25);
		getContentPane().add(btnCancelar);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cliente miCliente=clienteController.buscarCliente(textCodigo.getText());
				if (miCliente!=null)
				{
					muestraCliente(miCliente);
				}
				else if(ClienteServ.consultaCliente==true){
					JOptionPane.showMessageDialog(null, "La cliente no Existe","Advertencia",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnBuscar.setBounds(210, 86, 81, 18);
		getContentPane().add(btnBuscar);
		limpiar();
		
	}
	
	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}
	private void muestraCliente(Cliente miCliente) {
		textNombre.setText(miCliente.getNombre());
		textApellido.setText(miCliente.getApellido());
		textDireccion.setText(miCliente.getDireccion());
		textDNI.setText(miCliente.getDni() + "");
		habilita(true, false, false, false, false, true, false, true);
	}



	public void limpiar()	{
		textCodigo.setText("");
		textNombre.setText("");
		textApellido.setText("");
		textDireccion.setText("");
		textDNI.setText("");
		habilita(true, false, false, false, false, true, false, true);
	}


	public void habilita(boolean codigo, boolean nombre, boolean apellido, boolean direccion, boolean dni,	 boolean bBuscar, boolean bGuardar, boolean bModificar)	{
		textCodigo.setEditable(codigo);
		textNombre.setEditable(nombre);
		textApellido.setEditable(apellido);
		textDireccion.setEditable(direccion);
		textDNI.setEditable(dni);
		btnBuscar.setEnabled(bBuscar);
		btnGuardar.setEnabled(bGuardar);
		btnModificar.setEnabled(bModificar);
	}
	
	private void close() {
		this.dispose();
	}
	
}
