package views;

import javax.swing.*;


import java.awt.Font;

import controller.ClienteController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eliminar extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JTextField textCodigo;
	private ClienteController clienteController;
	
	public Eliminar() {
		setTitle("Vista eliminar");
		setBounds(100, 100, 450, 232);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblEliminarClientes = new JLabel("ELIMINAR CLIENTES");
		lblEliminarClientes.setBounds(95, 13, 246, 31);
		lblEliminarClientes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		getContentPane().add(lblEliminarClientes);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(118, 79, 56, 16);
		getContentPane().add(lblCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setText("");
		textCodigo.setEditable(true);
		textCodigo.setColumns(10);
		textCodigo.setBounds(186, 76, 97, 22);
		getContentPane().add(textCodigo);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eliminar();
			}
		});
		btnEliminar.setBounds(80, 132, 97, 25);
		getContentPane().add(btnEliminar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		btnCancelar.setBounds(250, 132, 97, 25);
		getContentPane().add(btnCancelar);
	}
	public void eliminar() {
		if (!textCodigo.getText().equals(""))
		{
			int respuesta = JOptionPane.showConfirmDialog(this,
					"Esta seguro de eliminar al Cliente?", "Confirmación",
					JOptionPane.YES_NO_OPTION);
			if (respuesta == JOptionPane.YES_NO_OPTION)
			{
				clienteController.eliminarCliente(textCodigo.getText());
				limpiar();
			}
		}
		else{
			JOptionPane.showMessageDialog(null, "Ingrese un numero de Documento", "Información",JOptionPane.WARNING_MESSAGE);
		}
	}
	public void limpiar()
	{
		textCodigo.setText("");
	}
	private void close() {
		this.dispose();
	}
	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}

}
