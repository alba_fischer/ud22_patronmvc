package views;



import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import controller.ClienteController;


public class MenuPrincipal extends JFrame{

	
	private static final long serialVersionUID = 1L;
	ClienteController cliente = new ClienteController();
	
	public MenuPrincipal() {
		setBounds(100, 100, 595, 268);
		setTitle("Vista Controlador Cliente");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JButton btnBuscar = new JButton("Buscar");
		
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				cliente.mostrarBuscar();
			}
		});
		btnBuscar.setBounds(306, 119, 220, 25);
		getContentPane().add(btnBuscar);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cliente.mostrarRegistrar();
			}
		});
		btnRegistrar.setBounds(50, 119, 220, 25);
		getContentPane().add(btnRegistrar);
		
		JLabel lblTitulo = new JLabel("PATRÓN MODELO VISTA CONTROLADOR");
		lblTitulo .setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTitulo .setBounds(64, 53, 472, 43);
		getContentPane().add(lblTitulo );
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cliente.mostrarEliminar();
			}
		});
		btnEliminar.setBounds(183, 168, 220, 25);
		getContentPane().add(btnEliminar);
		
		
	}
	
	public void setCoordinador(ClienteController clienteController) {
		this.cliente=clienteController;
	}
	
}
