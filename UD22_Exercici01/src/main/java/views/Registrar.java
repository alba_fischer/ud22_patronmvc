package views;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import model.dto.Cliente;
import controller.ClienteController;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Registrar extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textDNI;
	private JTextField textDireccion;
	private ClienteController clienteController;
	/**
	 * Initialize the contents of the frame.
	 */
	public Registrar() {
		setTitle("Vista registrar");
		setBounds(100, 100, 450, 260);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("REGISTRO DE CLIENTES");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTitulo.setBounds(80, 13, 278, 48);
		getContentPane().add(lblTitulo);
		
		textNombre = new JTextField();
		textNombre.setBounds(101, 80, 97, 22);
		getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(33, 83, 56, 16);
		getContentPane().add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(33, 127, 56, 16);
		getContentPane().add(lblApellido);
		
		textApellido = new JTextField();
		textApellido.setColumns(10);
		textApellido.setBounds(101, 124, 97, 22);
		getContentPane().add(textApellido);
		
		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setBounds(235, 83, 56, 16);
		getContentPane().add(lblDNI);
		
		JLabel lblDireccion = new JLabel("Dirección");
		lblDireccion.setBounds(235, 127, 56, 16);
		getContentPane().add(lblDireccion);
		
		textDNI = new JTextField();
		textDNI.setColumns(10);
		textDNI.setBounds(313, 80, 97, 22);
		getContentPane().add(textDNI);
		
		textDireccion = new JTextField();
		textDireccion.setColumns(10);
		textDireccion.setBounds(313, 124, 97, 22);
		getContentPane().add(textDireccion);
		
		JButton btnNewButton = new JButton("Registrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					try {
							Cliente miCliente=new Cliente();
							miCliente.setNombre(textNombre.getText());
							miCliente.setApellido(textApellido.getText());
							miCliente.setDireccion(textDireccion.getText());
							miCliente.setDni(Integer.parseInt(textDNI.getText()));
							
							clienteController.registrarCliente(miCliente);	
						} catch (Exception ex) {
							JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
							System.out.println(ex);
						}
					}
					
			
		});
		btnNewButton.setBounds(91, 175, 97, 25);
		getContentPane().add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		btnCancelar.setBounds(261, 175, 97, 25);
		getContentPane().add(btnCancelar);
		limpiar();
	}
	
	private void limpiar() 
	{
		textNombre.setText("");
		textApellido.setText("");
		textDireccion.setText("");
		textDNI.setText("");
	}
	
	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}
	private void close() {
		this.dispose();
	}
}
