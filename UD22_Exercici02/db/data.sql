DROP DATABASE `MVC_02`;
CREATE DATABASE IF NOT EXISTS `MVC_02`;

USE `MVC_02`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `fecha` date DEFAULT (CURRENT_DATE),
  PRIMARY KEY (`id`)
);

INSERT INTO `cliente` (nombre, apellido, direccion, dni) VALUES 
('Alba', 'Carles', 'Av Codonyol', '45896325'),
('Joan', 'Balada', 'Av Constitució', '32569854'),
('Raquel', 'Llorca', 'Av Catalunya', '45698235'),
('Esther', 'Gras', 'Passeig de Sant Joan', '75896321'),
('Arnau', 'Morancho', 'Av Paral·lel', '33652147'),
('Javier', 'Uviña', 'Av de Les Corts', '47859632'),
('Laia', 'Beltran', 'Plaça Catalunya', '15264352');


CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `director` varchar(250) DEFAULT NULL,
  `cli_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `videos_fk` FOREIGN KEY (`cli_id`) REFERENCES `cliente` (`id`)
);

INSERT INTO `videos` (title, director, cli_id) VALUES 
('After', 'Jenny Gage', '1'), 
('The Hunger Games', 'Gary Ross', '2'), 
('Divergent', 'Neil Burger', '3'), 
('Insurgent', 'Robert Schwentke', '4'), 
('Allegiant', 'Robert Schwentke', '5'), 
('Maze Runner', 'Wes Ball', '6'); 

