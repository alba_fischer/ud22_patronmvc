package UD22.UD22_Exercici02;


import controller.ClienteController;
import model.service.ClienteServ;
import controller.VideoController;
import model.service.VideoServ;
import views.BuscarCliente;
import views.EliminarCliente;
import views.MenuPrincipal;
import views.RegistrarCliente;
import views.BuscarVideo;
import views.EliminarVideo;
import views.RegistrarVideo;

public class MainApp {
	MenuPrincipal menuPrincipal;
	
	ClienteServ miclienteServ;
	BuscarCliente buscarCliente;
	RegistrarCliente registrarCliente;
	EliminarCliente eliminarCliente;
	ClienteController clienteController;

	VideoServ mivideoServ;
	BuscarVideo buscarVideo;
	RegistrarVideo registrarVideo;
	EliminarVideo eliminarVideo;
	VideoController videoController;
	
	
	public static void main(String[] args) {
		MainApp miPrincipal=new MainApp();
		miPrincipal.iniciar();
	}


private void iniciar() {
	/*Se instancian las clases*/
	menuPrincipal=new MenuPrincipal();
	
	registrarCliente = new RegistrarCliente();
	buscarCliente = new BuscarCliente();
	eliminarCliente = new EliminarCliente();
	miclienteServ=new ClienteServ();
	clienteController= new ClienteController();
	
	registrarVideo=new RegistrarVideo();
	buscarVideo= new BuscarVideo();
	eliminarVideo= new EliminarVideo();
	mivideoServ = new VideoServ();
	videoController = new VideoController();
		
	
	/*Se establecen las relaciones entre clases*/
	menuPrincipal.setCoordinador(clienteController);
	registrarCliente.setCoordinador(clienteController);
	buscarCliente.setCoordinador(clienteController);
	eliminarCliente.setCoordinador(clienteController);
	miclienteServ.setclienteController(clienteController);
	
	menuPrincipal.setVideoCoordinador(videoController);
	registrarVideo.setCoordinador(videoController);
	buscarVideo.setCoordinador(videoController);
	eliminarVideo.setCoordinador(videoController);
	mivideoServ.setvideoController(videoController);
	
	/*Se establecen relaciones con la clase coordinador*/
	clienteController.setMenuPrincipal(menuPrincipal);
	clienteController.setRegistrar(registrarCliente);
	clienteController.setEliminar(eliminarCliente);
	clienteController.setClienteServ(miclienteServ);
	clienteController.setBuscar(buscarCliente);		
	
	videoController.setMenuPrincipal(menuPrincipal);
	videoController.setRegistrarVideo(registrarVideo);
	videoController.setEliminarVideo(eliminarVideo);
	videoController.setVideoServ(mivideoServ);
	videoController.setBuscarVideo(buscarVideo);	
	
	menuPrincipal.setVisible(true);
}

}
