package controller;

import model.dto.Cliente;
import model.service.ClienteServ;
import views.BuscarCliente;
import views.MenuPrincipal;
import views.RegistrarCliente;
import views.EliminarCliente;


public class ClienteController {
	
	private ClienteServ clienteServ;
	private MenuPrincipal menuPrincipal;
	private RegistrarCliente registrar;
	private BuscarCliente buscar;
	private EliminarCliente eliminar;
	
	//Metodos getter Setters de vistas
	public MenuPrincipal getMenuPrincipal() {
		return menuPrincipal;
	}
	public void setMenuPrincipal(MenuPrincipal menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
	public RegistrarCliente getRegistrar() {
		return registrar;
	}
	public void setRegistrar(RegistrarCliente registrar) {
		this.registrar = registrar;
	}
	public BuscarCliente getBuscar() {
		return buscar;
	}
	public void setBuscar(BuscarCliente buscar) {
		this.buscar = buscar;
	}
	public EliminarCliente getEliminar() {
		return eliminar;
	}
	public void setEliminar(EliminarCliente eliminar) {
		this.eliminar = eliminar;
	}
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarRegistrar() {
		registrar.setVisible(true);
	}
	public void mostrarBuscar() {
		buscar.setVisible(true);
	}
	public void mostrarEliminar() {
		eliminar.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCliente(Cliente miCliente) {
		clienteServ.validarRegistro(miCliente);
	}
	
	public Cliente buscarCliente(String codigoCliente) {
		return clienteServ.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Cliente miCliente) {
		clienteServ.validarModificacion(miCliente);
	}
	
	public void eliminarCliente(String codigo) {
		clienteServ.validarEliminacion(codigo);
	}


}

