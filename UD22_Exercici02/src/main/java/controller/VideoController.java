package controller;

import model.dto.Video;
import model.service.VideoServ;
import views.BuscarVideo;
import views.MenuPrincipal;
import views.RegistrarVideo;
import views.EliminarVideo;


public class VideoController {
	
	private VideoServ videoServ;
	private MenuPrincipal menuPrincipal;
	private RegistrarVideo registrar;
	private BuscarVideo buscar;
	private EliminarVideo eliminar;
	
	//Metodos getter Setters de vistas
	public MenuPrincipal getMenuPrincipal() {
		return menuPrincipal;
	}
	public void setMenuPrincipal(MenuPrincipal menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
	public RegistrarVideo getMiVentanaRegistroVideos() {
		return registrar;
	}
	public void setRegistrarVideo(RegistrarVideo registrar) {
		this.registrar = registrar;
	}
	public BuscarVideo getBuscarVideo() {
		return buscar;
	}
	public void setBuscarVideo(BuscarVideo buscar) {
		this.buscar = buscar;
	}
	public EliminarVideo getEliminarVideo() {
		return eliminar;
	}
	public void setEliminarVideo(EliminarVideo eliminar) {
		this.eliminar = eliminar;
	}
	public VideoServ getVideoServ() {
		return videoServ;
	}
	public void setVideoServ(VideoServ videoServ) {
		this.videoServ = videoServ;
	}
	
	//Hace visible las vistas de registrar, buscar i eliminar
	public void mostrarRegistrarVideo() {
		registrar.setVisible(true);
	}
	public void mostrarBuscarVideo() {
		buscar.setVisible(true);
	}
	public void mostrarEliminarVideo() {
		eliminar.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarVideo(Video miVideo) {
		videoServ.validarRegistro(miVideo);
	}
	
	public Video buscarVideo(String codigoVideo) {
		return videoServ.validarConsulta(codigoVideo);
	}
	
	public void modificarVideo(Video miVideo) {
		videoServ.validarModificacion(miVideo);
	}
	
	public void eliminarVideo(String codigo) {
		videoServ.validarEliminacion(codigo);
	}


}
