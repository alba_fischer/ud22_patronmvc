package model.dao;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import model.conexion.Conexion;
import model.dto.Cliente;

/**
 * Clase que permite el acceso a la base de datos
 * CRUD
 *
 */
public class ClienteDao {

	public void registrarCliente(Cliente miCliente)
	{
		Conexion conex= new Conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO cliente (nombre, apellido, direccion, dni) VALUES " + "('" 
					+ miCliente.getNombre() + "', '"+ miCliente.getApellido() + "', '"
					+ miCliente.getDireccion() + "', '"+ miCliente.getDni() + "');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se registro");
		}
	}
	
	public Cliente buscarCliente(int codigo) 
	{
		Conexion conex= new Conexion();
		Cliente cliente= new Cliente();
		boolean existe=false;
		try {
			String sql= "SELECT * FROM cliente where id = ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			consulta.setInt(1, codigo);
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				existe=true;
				cliente.setIdCliente(Integer.parseInt(res.getString("id")));
				cliente.setNombre(res.getString("nombre"));
				cliente.setApellido(res.getString("apellido"));
				cliente.setDireccion(res.getString("direccion"));
				cliente.setDni(Integer.parseInt(res.getString("dni")));
			 }
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return cliente;
			}
			else return null;				
	}
	
	public void modificarCliente(Cliente miCliente) {
		
		Conexion conex= new Conexion();
		try{
			String consulta="UPDATE cliente SET id= ? ,nombre = ? , apellido=? , direccion=? , dni= ?, fecha=(CURRENT_DATE) WHERE id= ? ";
			PreparedStatement estatuto = conex.getConnection().prepareStatement(consulta);
			
            estatuto.setInt(1, miCliente.getIdCliente());
            estatuto.setString(2, miCliente.getNombre());
            estatuto.setString(3, miCliente.getApellido());
            estatuto.setString(4, miCliente.getDireccion());
            estatuto.setInt(5,miCliente.getDni());
            estatuto.setInt(6, miCliente.getIdCliente());
            estatuto.executeUpdate();
            
          JOptionPane.showMessageDialog(null, " Se ha modificado correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
          System.out.println(consulta);
         

        }catch(SQLException	 e){

            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Error al modificar","Error",JOptionPane.ERROR_MESSAGE);

        }
	}
	
	public void eliminarCliente(String codigo)
	{
		Conexion conex= new Conexion();
		try {
			String sql= "DELETE FROM cliente WHERE id='"+codigo+"'";
			Statement st = conex.getConnection().createStatement();
			st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, " Se ha eliminado correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
            System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se elimino");
		}
	}
}
