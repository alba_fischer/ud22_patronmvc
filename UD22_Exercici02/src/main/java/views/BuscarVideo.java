package views;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.VideoController;
import model.dto.Video;
import model.service.VideoServ;

public class BuscarVideo extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField textCodigo;
	private JTextField textTitulo;
	private JTextField textDirector;
	private JTextField textCli_id;
	private VideoController videoController;
	private JButton btnModificar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JButton btnCancelar;
	
	public BuscarVideo() {
		setTitle("Vista buscar");
		setBounds(100, 100, 450, 326);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblAdministrarVideos = new JLabel("ADMINISTRAR VÍDEOS");
		lblAdministrarVideos.setBounds(70, 27, 299, 46);
		lblAdministrarVideos.setFont(new Font("Tahoma", Font.PLAIN, 25));
		getContentPane().add(lblAdministrarVideos);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(33, 86, 56, 16);
		getContentPane().add(lblCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setText("");
		textCodigo.setColumns(10);
		textCodigo.setBounds(101, 83, 97, 22);
		getContentPane().add(textCodigo);
		
		JLabel lblTitulo = new JLabel("Título");
		lblTitulo.setBounds(33, 132, 56, 16);
		getContentPane().add(lblTitulo);
		
		textTitulo = new JTextField();
		textTitulo.setText("");
		textTitulo.setColumns(10);
		textTitulo.setBounds(101, 129, 109, 22);
		getContentPane().add(textTitulo);
		
		JLabel lblCli_id = new JLabel("Cli_id");
		lblCli_id.setBounds(235, 132, 56, 16);
		getContentPane().add(lblCli_id);
		
		textCli_id = new JTextField();
		textCli_id.setText("");
		textCli_id.setColumns(10);
		textCli_id.setBounds(303, 129, 107, 22);
		getContentPane().add(textCli_id);
		
		JLabel lblDirector = new JLabel("Dirección");
		lblDirector.setBounds(33, 179, 56, 16);
		getContentPane().add(lblDirector);
		
		textDirector = new JTextField();
		textDirector.setText("");
		textDirector.setColumns(10);
		textDirector.setBounds(101, 176, 109, 22);
		getContentPane().add(textDirector);
	
		
		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilita(false, true, true, true, false, true, false);
				
			}
		});
		btnModificar.setBounds(166, 228, 97, 25);
		getContentPane().add(btnModificar);
		
		btnGuardar = new JButton();
		btnGuardar.setText("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				{
					try {				
						Video miVideo=new Video();
						miVideo.setIdVideo(Integer.parseInt(textCodigo.getText()));
						miVideo.setTitle(textTitulo.getText());
						miVideo.setDirector(textDirector.getText());
						miVideo.setCli_id(Integer.parseInt(textCli_id.getText()));
						

						videoController.modificarVideo(miVideo);
						
						if (VideoServ.modificaVideo==true) {
							habilita(true, false, false, false, true, false, true);	
						}
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null,"Error en el ingreso de datos","Error",JOptionPane.ERROR_MESSAGE);
						System.out.println(e2);
					}
					
				}
			}
		});
		btnGuardar.setBounds(57, 228, 97, 25);
		getContentPane().add(btnGuardar);
		
		btnCancelar = new JButton();
		btnCancelar.setText("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		btnCancelar.setBounds(275, 228, 97, 25);
		getContentPane().add(btnCancelar);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Video miVideo=videoController.buscarVideo(textCodigo.getText());
				if (miVideo!=null)
				{
					muestraVideo(miVideo);
				}
				else if(VideoServ.consultaVideo==true){
					JOptionPane.showMessageDialog(null, "El video no existe","Advertencia",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnBuscar.setBounds(210, 86, 81, 18);
		getContentPane().add(btnBuscar);
		limpiar();
		
	}
	
	public void setCoordinador(VideoController videoController) {
		this.videoController=videoController;
	}
	
	/**
	 * permite cargar los datos de la video consultada
	 * @param miVideo
	 */
	private void muestraVideo(Video miVideo) {
		textTitulo.setText(miVideo.getTitle());
		textDirector.setText(miVideo.getDirector());
		textCli_id.setText(miVideo.getCli_id()+"");
		habilita(true, false, false, false, true, false, true);
	}

	public void limpiar()	{
		textCodigo.setText("");
		textTitulo.setText("");
		textDirector.setText("");
		textCli_id.setText("");
		habilita(true, false, false, false, true, false, true);
	}
	
	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * @param codigo
	 * @param nombre
	 * @param dni
	 * @param tel
	 * @param direccion
	 * @param cargo
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */

	public void habilita(boolean codigo, boolean titulo,  boolean director, boolean Cli_id,	boolean bBuscar, boolean bGuardar, boolean bModificar)	{
		textCodigo.setEditable(codigo);
		textTitulo.setEditable(titulo);
		textDirector.setEditable(director);
		textCli_id.setEditable(Cli_id);
		btnBuscar.setEnabled(bBuscar);
		btnGuardar.setEnabled(bGuardar);
		btnModificar.setEnabled(bModificar);
	}
	
	private void close() {
		this.dispose();
	}
	
}
