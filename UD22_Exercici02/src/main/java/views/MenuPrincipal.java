package views;



import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import controller.VideoController;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import controller.ClienteController;


public class MenuPrincipal extends JFrame{

	private VideoController video = new VideoController();
	private static final long serialVersionUID = 1L;
	private ClienteController cliente = new ClienteController();
	
	public MenuPrincipal() {
		setBounds(100, 100, 595, 268);
		setTitle("Vista Controlador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JButton btnBuscar = new JButton("Buscar Cliente");
		
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cliente.mostrarBuscar();
			}
		});
		btnBuscar.setBounds(217, 119, 155, 25);
		getContentPane().add(btnBuscar);
		
		JButton btnRegistrar = new JButton("Registrar Cliente");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cliente.mostrarRegistrar();
			}
		});
		btnRegistrar.setBounds(50, 119, 155, 25);
		getContentPane().add(btnRegistrar);
		
		JLabel lblTitulo = new JLabel("PATRÓN MODELO VISTA CONTROLADOR");
		lblTitulo .setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTitulo .setBounds(64, 53, 472, 43);
		getContentPane().add(lblTitulo );
		
		JButton btnEliminar = new JButton("Eliminar Cliente");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cliente.mostrarEliminar();
			}
		});
		btnEliminar.setBounds(387, 119, 149, 25);
		getContentPane().add(btnEliminar);
		
		JButton btnRegistrarVideo = new JButton("Registrar Vídeo");
		btnRegistrarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				video.mostrarRegistrarVideo();
				
			}
		});
		btnRegistrarVideo.setBounds(50, 168, 155, 25);
		getContentPane().add(btnRegistrarVideo);
		
		JButton btnBuscarVideo = new JButton("Buscar Vídeo");
		btnBuscarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				video.mostrarBuscarVideo();
			}
		});
		btnBuscarVideo.setBounds(217, 168, 155, 25);
		getContentPane().add(btnBuscarVideo);
		
		JButton btnEliminarVideo = new JButton("Eliminar Vídeo");
		btnEliminarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				video.mostrarEliminarVideo();
			}
		});
		btnEliminarVideo.setBounds(387, 168, 155, 25);
		getContentPane().add(btnEliminarVideo);
		
		
	}
	
	public void setCoordinador(ClienteController clienteController) {
		this.cliente=clienteController;
	}
	public void setVideoCoordinador(VideoController videoController) {
		this.video=videoController;
	}
}
