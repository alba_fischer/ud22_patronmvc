package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import model.dto.Video;
import controller.VideoController;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegistrarVideo extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JTextField textTitle;
	private JTextField textCli_id;
	private JTextField textDirector;
	private VideoController videoController;
	/**
	 * Initialize the contents of the frame.
	 */
	public RegistrarVideo() {
		setTitle("Vista registrar");
		setBounds(100, 100, 450, 260);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("REGISTRO DE VIDEOS");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTitulo.setBounds(80, 13, 278, 48);
		getContentPane().add(lblTitulo);
		
		textTitle = new JTextField();
		textTitle.setBounds(101, 80, 97, 22);
		getContentPane().add(textTitle);
		textTitle.setColumns(10);
		
		JLabel lblTitle = new JLabel("Título");
		lblTitle.setBounds(33, 83, 56, 16);
		getContentPane().add(lblTitle);
		
		JLabel lblCli_di = new JLabel("Cli_id");
		lblCli_di.setBounds(235, 83, 56, 16);
		getContentPane().add(lblCli_di);
		
		JLabel lblDirector= new JLabel("Director");
		lblDirector.setBounds(33, 127, 56, 16);
		getContentPane().add(lblDirector);
		
		textCli_id = new JTextField();
		textCli_id.setColumns(10);
		textCli_id.setBounds(313, 80, 97, 22);
		getContentPane().add(textCli_id);
		
		textDirector = new JTextField();
		textDirector.setColumns(10);
		textDirector.setBounds(101, 124, 97, 22);
		getContentPane().add(textDirector);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					try {
							Video miVideo=new Video();
							miVideo.setTitle(textTitle.getText());
							miVideo.setDirector(textDirector.getText());
							miVideo.setCli_id(Integer.parseInt(textCli_id.getText()));
							
							videoController.registrarVideo(miVideo);	
						} catch (Exception ex) {
							JOptionPane.showMessageDialog(null,"Error en el ingreso de datos","Error",JOptionPane.ERROR_MESSAGE);
							System.out.println(ex);
						}
					}
					
			
		});
		btnRegistrar.setBounds(91, 175, 97, 25);
		getContentPane().add(btnRegistrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		btnCancelar.setBounds(261, 175, 97, 25);
		getContentPane().add(btnCancelar);
		limpiar();
	}
	
	private void limpiar() 
	{
		textCli_id.setText("");
		textTitle.setText("");
		textDirector.setText("");
	}
	
	public void setCoordinador(VideoController videoController) {
		this.videoController=videoController;
	}
	private void close() {
		this.dispose();
	}
}

